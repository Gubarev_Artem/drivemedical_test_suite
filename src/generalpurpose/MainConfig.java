package generalpurpose;


public class MainConfig {
    public static int default_timeout = 60;
    public static int amount_product_for_test = 10;
    public static int limit_attachments = 50;
    public static String main_page_b2b_url = "http://www.drivemedical.com/b2b/";
    public static String main_page_consumer_url = "http://www.drivemedical.com/consumer/";
    //public static String main_page_b2b_url = "http://dev.drivemedical.com/b2b";
    public static String login_consumer = "webmaster";
    public static String password_consumer = "magento1";
    public static String title_product_catalog_xpath = "//div[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'), 'product catalog')]";
    // email data
    public static String subject = "Results tests in drivemedical";
    public static String smtpHost="smtp.gmail.com";
    public static String login="testercyberhull@gmail.com";
    public static String password_email ="selenium";
    public static String smtpPort="587";
    // data
    public static String login_button_xpath = "//img[@alt = 'LOGIN']";
    public static String email_id = "email";
    public static String password_id = "pass";
    public static String submit_button_id = "send2";
    public static String logout_link_xpath = "//img[@alt = 'LOGOUT']";
}