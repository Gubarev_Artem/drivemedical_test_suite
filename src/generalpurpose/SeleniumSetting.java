package generalpurpose;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SeleniumSetting extends Assert {
    public WebDriver driver;
    public WebDriverWait wait_browser;
    public void seleniumSetup( String browser) {
        FirefoxProfile profile = new FirefoxProfile();
        final String GA_disabled = "gaoptoutaddon_0.9.6.xpi";
        try {
            profile.addExtension(new File(GA_disabled));
        } catch (IOException e) {
            e.printStackTrace();
        }
        profile.setPreference( "network.http.response.timeout", MainConfig.default_timeout);
        String os = System.getProperty("os.name").toLowerCase();
        if ( browser.equals( "Firefox")) {
            driver = new FirefoxDriver( profile);
        }
        else if ( browser.equals("Chrome") & os.contains("win")) {
            System.setProperty( "webdriver.chrome.driver", "C:\\chromedriver.exe");
            driver = new ChromeDriver();
        }
        else if ( browser.equals("Chrome") & os.contains("nux")) {
            System.setProperty( "webdriver.chrome.driver", "/home/selenium/chromedriver.exe");
            driver = new ChromeDriver();
        }
        else {
            assertTrue( false, "Not selected the default browser");
        }
        driver.manage().timeouts().pageLoadTimeout(MainConfig.default_timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(MainConfig.default_timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(MainConfig.default_timeout, TimeUnit.SECONDS);
        try {
            driver.get( MainConfig.main_page_b2b_url);
        }
        catch (org.openqa.selenium.TimeoutException e) {
            System.out.println( "Site DriveMedical is not available" );
            driver.quit();
            return;
        }
        if( checkurl( driver) == false) {
            String message = "REQUEST URL (%s) and GETTING URL (%s) are different";
            System.out.println( message.format("REQUEST URL (%s) and GETTING URL (%s) are different", MainConfig.main_page_b2b_url, driver.getCurrentUrl()));
            driver.quit();

        }
        wait_browser = new WebDriverWait( driver, MainConfig.default_timeout );
        return;
    }

    public void setDriverTimeout () {
        driver.manage().timeouts().pageLoadTimeout(MainConfig.default_timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(MainConfig.default_timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(MainConfig.default_timeout, TimeUnit.SECONDS);
    }

    private static boolean checkurl ( WebDriver driver) {
        String url = driver.getCurrentUrl();
        if ( url.contains( MainConfig.main_page_b2b_url )) {
            return true;
        }
        return false;
    }

    public void restartWebDriver ( String browser ) {
        driver.close();
        seleniumSetup( browser );
    }
}

