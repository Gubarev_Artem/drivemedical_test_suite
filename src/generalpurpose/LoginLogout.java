package generalpurpose;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Artem.Gubarev on 19.08.2014.
 */
public class LoginLogout {
    public static boolean loginDriveMedical( SeleniumSetting browser_driver, String login, String password ) {
        GoToPage.goTogoToMainPageB2B( browser_driver );
        try {
            browser_driver.driver.findElement( By.xpath( MainConfig.login_button_xpath )).click();
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("Can't find login link on top DriveMedical page");
            return false;
        }
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.id( MainConfig.email_id )));
        browser_driver.driver.findElement( By.id( MainConfig.email_id )).sendKeys( login );
        browser_driver.driver.findElement( By.id( MainConfig.password_id )).sendKeys( password );
        browser_driver.driver.findElement( By.id( MainConfig.submit_button_id )).click();
        try{
            browser_driver.driver.findElement( By.xpath(MainConfig.logout_link_xpath));
            return true;
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public static boolean logoutDriveMedical( SeleniumSetting browser_driver ) {
        try {
            browser_driver.driver.findElement( By.xpath(MainConfig.logout_link_xpath)).click();
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("Can't find button logout on top DriveMedical page");
            return false;
        }
        try {
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( MainConfig.login_button_xpath)));
            return true;
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }
}
