package generalpurpose;

public class GoToPage {
    public static void goTogoToMainPageB2B( SeleniumSetting browser_driver) {
    browser_driver.driver.get( MainConfig.main_page_b2b_url );
    }

    public static void goTogoToMainPageConsumer( SeleniumSetting browser_driver) {
        browser_driver.driver.get( MainConfig.main_page_consumer_url);
    }
}
