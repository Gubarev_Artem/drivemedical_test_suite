package functionaltests;

import generalpurpose.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.*;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DriveMedicalTest extends Assert {
    SeleniumSetting browser_driver;
    private ArrayList<String> name_models = new ArrayList<String>();

    @Parameters({"browser"})
    @BeforeMethod
    public void setUp( String browser ) {
        browser_driver = new SeleniumSetting();
        browser_driver.seleniumSetup( browser );
    }

    @AfterClass
    public void tearDown() {
        browser_driver.driver.quit();
    }

    @AfterMethod( alwaysRun = false )
    public void catchExceptions( ITestResult result) {
        MakeScreenshot.screenshotFailTest( result, browser_driver );
    }

    @AfterMethod( alwaysRun = true )
    public void quitDriver() {
        browser_driver.driver.close();
    }

    @Test( description = "Test login in DriveMedical", enabled = false )
    public void testLoginDriveMedical() throws Exception {
        String email = Randoms.randomString(10) + "@gmail.com";
        String password = Randoms.randomString(10);
        assertFalse(LoginLogout.loginDriveMedical(browser_driver, email, password),
                "Login with wrong email and password in DriveMedical");
        assertFalse( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, password ),
                "Login with wrong password in DriveMedical");
        assertTrue( LoginLogout.loginDriveMedical(browser_driver, MainConfig.login_consumer, MainConfig.password_consumer),
                "Can`t login in DriveMedical with provided credentials");
    }

    @Test( description = "Test logout in DriveMedical", enabled = false )
    public void testLogoutDriveMedical() throws Exception {
        assertTrue( LoginLogout.loginDriveMedical( browser_driver,MainConfig.login_consumer, MainConfig.password_consumer ),
                "Can`t login in DriveMedical with provided credentials");
        assertTrue( LoginLogout.logoutDriveMedical(browser_driver), "Not happened Logout or exit message been changed" );
    }

    @Test( description = "Test search in DriveMedical", enabled = false )
    public void testSearch() throws Exception {
        GoToPage.goTogoToMainPageB2B( browser_driver );
        // receive products categories
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.left_product_menu_xpath )));
        List<WebElement> list_product_categories = browser_driver.driver.findElements( By.xpath( Data.left_product_menu_xpath ));
        if (list_product_categories.size() == 0) {
            // Add message in report
            System.out.println( "Categories product catalog are not found. Checking the images is not made. ");
            System.exit(0);
        }
        int amount_product_categories = list_product_categories.size();
        for (int number_category = 0; number_category < amount_product_categories; number_category++) {
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.left_product_menu_xpath)));
            list_product_categories = browser_driver.driver.findElements( By.xpath( Data.left_product_menu_xpath ));
            WebElement category = list_product_categories.get( number_category);
            category.click();
            try {
                browser_driver.wait_browser.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Data.left_secondary_product_menu_xpath)));
            }
            catch (org.openqa.selenium.TimeoutException e) {
                System.exit(0);
            }
            // Choice  and find product
            browser_driver.driver.findElement( By.xpath( Data.featured_products_xpath )).click();
            String product_name = browser_driver.driver.findElement( By.xpath( "//h1" )).getText();
            GoToPage.goTogoToMainPageB2B(browser_driver);
            browser_driver.driver.findElement( By.id(Data.search_input_id)).sendKeys( product_name );
            browser_driver.driver.findElement( By.xpath(Data.button_search_xpath)).click();
            try {
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.first_product_in_productlist_xpath )));
            }
            catch (org.openqa.selenium.TimeoutException e) {
                System.out.println("Product is not founded");
            }
            GoToPage.goTogoToMainPageB2B(browser_driver);
        }
        // find random text
        String product_name = Randoms.randomString(12);
        browser_driver.driver.findElement( By.id( Data.search_input_id )).sendKeys( product_name );
        browser_driver.driver.findElement( By.xpath( Data.button_search_xpath )).click();
        try {
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.message_not_find_product_xpath )));
        }
        catch (org.openqa.selenium.TimeoutException e) {
            System.out.println( "Search for a random set of letters found the product" );
            assertTrue( false, "Search is not worked." );
        }
    }

    @Test( description = "Test consumer menu in DriveMedical", enabled = false )
    public void testCheckConsumerMenu() throws Exception {
        assertFalse(check_links_menu( Data.top_menu_links_xpath, MainConfig.main_page_consumer_url ), "Consumer top menu is not worked");
        assertFalse(check_links_menu( Data.bottom_menu_links_xpath, MainConfig.main_page_consumer_url ), "Consumer bottom menu is not worked");

    }

    @Test( description = "Test provider menu in DriveMedical", enabled = false )
    public void testCheckProviderMenu() throws Exception {
        assertTrue( LoginLogout.loginDriveMedical( browser_driver,MainConfig.login_consumer, MainConfig.password_consumer ),
                "Can`t login in DriveMedical with provided credentials");
        assertFalse(check_links_menu( Data.top_menu_links_xpath, MainConfig.main_page_b2b_url ), "Provider top menu is not worked");
        assertFalse(check_links_menu( Data.bottom_menu_links_xpath, MainConfig.main_page_b2b_url ), "Provider bottom menu is not worked");
    }

    @Test( description = "Test add to cart in DriveMedical", enabled = true )
    public void testCheckAddToCart() throws Exception {
        assertTrue( LoginLogout.loginDriveMedical( browser_driver,MainConfig.login_consumer, MainConfig.password_consumer ),
                "Can`t login in DriveMedical with provided credentials");
        for (int number_product = 0;number_product < MainConfig.amount_product_for_test; number_product++) {
            addRandomProductToCart();
        }
        checkProductInCart();
    }

    @Test( description = "Test dell from cart in DriveMedical", enabled = true )
    public void testDelToCart() throws Exception {
        assertTrue( LoginLogout.loginDriveMedical( browser_driver,MainConfig.login_consumer, MainConfig.password_consumer ),
                "Can`t login in DriveMedical with provided credentials");
        for (int number_product = 0;number_product < MainConfig.amount_product_for_test; number_product++) {
            addRandomProductToCart();
        }
        checkProductInCart();
        
    }

    private boolean check_links_menu( String menu_links, String main_page ) {
        browser_driver.driver.get(main_page);
        boolean flag = false;
        List<WebElement> list_top_menu = browser_driver.driver.findElements(By.xpath(menu_links));
        if (list_top_menu.size() == 0) {
            System.out.println("Top menu is not find");
            assertTrue(false, "Top menu is not find");
            return flag;
        }
        int amount_link_menu = list_top_menu.size();
        for (int number_link = 0; number_link < amount_link_menu; number_link++) {
            list_top_menu = browser_driver.driver.findElements(By.xpath(menu_links));
            WebElement link_menu = list_top_menu.get(number_link);
            String name_menu = link_menu.getText();
            link_menu.click();
            String content = "";
            try {
                content = browser_driver.driver.findElement(By.xpath(Data.content_page_xpath)).getText();
                if (content.length() < 100) {
                    flag = true;
                    System.out.println("Content page is little");
                }
            } catch (NoSuchElementException e) {
                flag = true;
                System.out.println("Link menu " + name_menu + "is not worked");
            }
            browser_driver.driver.get(main_page);
        }
    return flag;
    }

    private void checkProductInCart() throws Exception {
        boolean flag = false;
        String back_url = browser_driver.driver.getCurrentUrl();
        System.out.println( back_url+ "\n");
        browser_driver.driver.findElement( By.xpath( Data.button_cart_xpath )).click();
        List<WebElement> list_products = browser_driver.driver.findElements( By.xpath( Data.products_models_in_cart_xpath ));
        if (list_products.size() == 0) {
            // Add message in report
            System.out.println( "Products in cart are not found. Stop test.");
            System.exit(0);
        }
        for (int number_product = 0;number_product < MainConfig.amount_product_for_test; number_product++) {
            list_products = browser_driver.driver.findElements( By.xpath( Data.products_models_in_cart_xpath ));
            WebElement name_product = list_products.get(number_product);
            String name = name_product.getAttribute("value");
            if (!name_models.contains(name)) {
                System.out.println( "Product: " + name + "are not found in cart\n");
                System.out.println( "array: " + name_models.get(number_product) + "\n");
                flag = true;
            }
        }
        assertFalse(flag, "Not all added products are found in cart");
    }

    private void addRandomProductToCart() throws Exception {
        GoToPage.goTogoToMainPageB2B( browser_driver );
        // receive products categories
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.left_product_menu_xpath )));
        List<WebElement> list_product_categories = browser_driver.driver.findElements( By.xpath( Data.left_product_menu_xpath ));
        if (list_product_categories.size() == 0) {
            // Add message in report
            System.out.println( "Categories product catalog are not found. Stop test.");
            System.exit(0);
        }
        int amount_category = list_product_categories.size();
        Random rand = new Random();
        int number_category = rand.nextInt(amount_category);
        WebElement category = list_product_categories.get(number_category);
        try {
            category.click();
        }
        catch (org.openqa.selenium.TimeoutException e) {
            addRandomProductToCart();
            return;
        }
        try {
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.left_secondary_product_menu_xpath )));
        }
        catch (org.openqa.selenium.TimeoutException e) {
            System.exit(0);
        }
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.left_secondary_product_menu_xpath )));
        List<WebElement> list_product_subcategories = browser_driver.driver.findElements( By.xpath( Data.left_secondary_product_menu_xpath ));
        if (list_product_subcategories.size() == 0) {
            // Add message in report
            System.out.println( "Subcategories product catalog are not found.");
        }
        int amount_product_subcategories = list_product_subcategories.size();
        int number_subcategory = rand.nextInt(amount_product_subcategories);
        WebElement subcategory = list_product_subcategories.get(number_subcategory);
        try {
            subcategory.click();
        }
        catch (org.openqa.selenium.TimeoutException e) {
            addRandomProductToCart();
            return;
        }
        browser_driver.wait_browser.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Data.left_product_menu_xpath )));
        showAllProducts();
        // Get list of products and list of names products
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.title_product_catalog )));
        List<WebElement> list_products = browser_driver.driver.findElements( By.xpath( Data.products_xpath ));
        if (list_products.size() == 0) {
        // If is not products.
            choiceRandomProductInSubdirectoryDepthOfFour();
        }
        else {
            choiceRandomInListProducts();
        }
    }

    private void choiceRandomProductInSubdirectoryDepthOfFour() {
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.third_level_category_products_xpath)));
        List<WebElement> list_product_subcategories = browser_driver.driver.findElements( By.xpath( Data.third_level_category_products_xpath));
        if (list_product_subcategories.size() == 0) {
            // Add message in report
            System.out.println("Product subcategories or products are not founded.");
            return;
        }
        int amount_subcategories = list_product_subcategories.size();
        Random rand = new Random();
        int number_subcategory = rand.nextInt(amount_subcategories);
        WebElement subcategory = list_product_subcategories.get(number_subcategory);
        subcategory.click();
        List<WebElement> list_products = browser_driver.driver.findElements( By.xpath( Data.products_xpath));
        if (list_products.size() == 0) {
            // If is not products.
            choiceRandomProductInSubdirectoryDepthOfFour();
        }
        else {
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.models_products_xpath)));
            showAllProducts();
            choiceRandomInListProducts();
        }
    }

    private void showAllProducts() {
        // Select show All product. If not go on
        String back_url = browser_driver.driver.getCurrentUrl();
        try {
            browser_driver.driver.findElement( By.xpath( Data.amount_show_all_product_xpath)).click();
        }
        catch (org.openqa.selenium.TimeoutException e) {
            browser_driver.driver.get( back_url );
            showAllProducts();
            return;
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
            // Does not exist element select All product.
        }
    }

    private void choiceRandomInListProducts() {
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.title_product_catalog )));
        List<WebElement> list_products = browser_driver.driver.findElements( By.xpath( Data.products_xpath ));
        if (list_products.size() == 0) {
            // Add message in report
            System.out.println("List products are empty. ");
            System.exit(0);
        }
        int amount_products = list_products.size();
        // Check images in list products
        String back_url = browser_driver.driver.getCurrentUrl();
        Random rand = new Random();
        int number_product = rand.nextInt(amount_products);
        WebElement product = list_products.get(number_product);
        // Enter in product page
        product.click();
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.header_product_xpath )));
        String name_product = browser_driver.driver.findElement( By.xpath( Data.products_sku_xpath )).getText();
        List<WebElement> list_buttons_buy = browser_driver.driver.findElements( By.xpath( Data.button_buy_now_xpath ));
        if (list_buttons_buy.size() == 0) {
            // Add message in report
            System.out.println("No prices on the product page.");
            // Back to product list
            browser_driver.driver.get(back_url);
            }
        // Choice product price on the page
        // Store the current window handle
        String winHandleBefore = browser_driver.driver.getWindowHandle();
        int number_button = 0;
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( Data.header_product_xpath )));
        list_buttons_buy = browser_driver.driver.findElements( By.xpath( Data.button_buy_now_xpath ));
        WebElement button_price = list_buttons_buy.get(number_button);
        button_price.click();
        for ( String winHandle : browser_driver.driver.getWindowHandles()) {
            browser_driver.driver.switchTo().window(winHandle);
        }
        //Check price
        String price = browser_driver.driver.findElement( By.xpath( Data.price_xpath )).getText();
        Pattern p = Pattern.compile("(\\$[0-9]+.[0-9]{2})");
        Matcher m = p.matcher(price);
        boolean b = m.matches();
        if (b) {
            // Add product to cart
            try {
                browser_driver.driver.findElement( By.xpath( Data.button_add_to_cart_xpath )).click();
            }
            catch (Exception e) {
                assertTrue(false, "Product is not add to cart");
                return;
            }
            name_models.add(name_product);
        }
        // Switch back to original browser (first window)
        browser_driver.driver.switchTo().window(winHandleBefore);
        // Check visibility main page
        do {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            b = browser_driver.driver.findElement( By.xpath( Data.button_buy_now_xpath )).isDisplayed();
        }
        while (!b);
    }
}

