package functionaltests;


public class Data {

    public static String search_input_id = "search";
    public static String left_product_menu_xpath = "//ul[@id='business-menu']//a[@title]/span";
    public static String left_secondary_product_menu_xpath = "//div[@class='n2-banner-right']/ul/li/a";
    public static String featured_products_xpath = "//ul[@id='product-carousel']/li[1]//img";
    public static String button_search_xpath = "//input[2][@class='submit-mg'][@type='submit']";
    public static String first_product_in_productlist_xpath = "//ol[@id='products-list']//h2[1]/a";
    public static String message_not_find_product_xpath = "//p[@class='note-msg']";
    // data test menu
    public static String top_menu_links_xpath = "//ul[@id='nav']/li/a";
    public static String bottom_menu_links_xpath = "//div[@class='footer']//ul[@class='links']//a";
    public static String content_page_xpath = "//div[@class='col-main']";

    // data test product add to cart
    public static String amount_show_all_product_xpath = "//select/option[contains(text(),'All')]";
    public static String header_product_xpath = "//div[@class='product-name']/h1";
    public static String products_xpath = "//ol[@id='products-list']//a/img";
    public static String names_products_xpath = "//ol[@id='products-list']//h2/a";
    public static String title_product_catalog = "//div[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'), 'product catalog')]";
    public static String third_level_category_products_xpath = "//div[@class='product-image']/a/img";
    public static String names_third_level_category_products_xpath = "//h6/a";
    public static String message_no_product_xpath = "//p[@class='note-msg'][contains (text(), 'There are no products matching the selection.')]";
    public static String models_products_xpath = "//span[@class = 'price']/span";
    public static String button_buy_now_xpath = "//span[@class='pdp-block pdp-btn-desc']";
    public static String price_xpath = "//div[contains (@class,'link-myprice')]";
    public static String button_close_window_xpath = "//a[@id='fancybox-close']";
    public static String button_add_to_cart_xpath = "//div[@id='fancybox-content']//button[@id='pdp-cart-btn']";
    public static String button_cart_xpath = "//img[@alt='CART']";
    public static String products_models_in_cart_xpath = "//table[@id='cart_items']/tbody//td[2]/input[1]";
    public static String products_sku_xpath = "//table[@class='pdp-prod-list']//td[@class='product-sku']";
}
