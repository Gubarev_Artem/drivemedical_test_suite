package generalpurpose;

import java.util.Random;

public class Randoms {
    public static String randomString( int chars) {
        char[] symbols = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < chars; i++) {
            char c = symbols[random.nextInt(symbols.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static String randomInt( int count) {
        char[] symbols = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            char c = symbols[random.nextInt(symbols.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
